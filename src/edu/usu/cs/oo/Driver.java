package edu.usu.cs.oo;

public class Driver {

	public static void main(String[] args) {
		Student student = new Student("Delin Davis", "A02054378", new Job(
				"Research Assistant", 24000,
				new Company("Utah State Unversity", new Location(
						"Old Main Hill", "84321", "Logan", "Utah"))));

		/*
		 * Instantiate an instance of Student that includes your own personal
		 * information Feel free to make up information if you would like.
		 */

		System.out.println(student);

		/*
		 * Print out the student information.
		 */
	}

}