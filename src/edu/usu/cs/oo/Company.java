package edu.usu.cs.oo;

public class Company {

	private String name;
	private Location location;

	public Company(String name, Location location) {
		super();
		this.name = name;
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return name + ", " + location.getCity();
	}

	/*
	 * Create the constructor, getters, setters, and anything else that is
	 * necessary to make Company work.
	 * 
	 * Note: This includes the toString() method.
	 */

}
